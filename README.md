###Hopper###

O repositório Hopper contém todos os trabalhos desenvolvidos na Cadeira de Compiladores
Versão Final

### Trabalhos desenvolvidos ###

* Implementação de um Compilador, Interpretador e Maquina Virtual para Expressões Aritméticas Simples
* Analisador Léxico implementado em Lex
* Analisador Léxico para a linguagem Pascal
* Gerador de Código com 3 Endereços em Javacc