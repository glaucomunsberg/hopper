DIGITO	[0-9]
LETRA	[a-zA-Z]
ID	{LETRA}({LETRA}|{DIGITO})*
EXPOENTE ("E"("+"|"-")?)
RESERVADA ("PROGRAM"|"program")
%%
{RESERVADA}		{printf("RESERVADA: %s\n",yytext);}
{ID}			{printf("var: %s\n",yytext);}	
{DIGITO}+("."{DIGITO}+)?({EXPOENTE}{DIGITO}+)? {printf("NUM: %s\n",yytext);}

%%
int yywrap();

main(){
  yylex();
}

int yywrap(){
 return 1;
}
