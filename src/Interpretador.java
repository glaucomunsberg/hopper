import java.util.LinkedList;


public class Interpretador {
	LinkedList<Float> values = new LinkedList<Float>();
	Float result;
	String executa (ArvoreSintatica arv)
	{
		return Float.toString(executaRecursivo(arv));
	}
	
	Float executaRecursivo (ArvoreSintatica arv)
	{
		if (arv instanceof Mult)
			return executaRecursivo(((Mult) arv).arg1) * executaRecursivo(((Mult) arv).arg2);

		if (arv instanceof Soma)
			return executaRecursivo(((Soma) arv).arg1) + executaRecursivo(((Soma) arv).arg2);
				
		if (arv instanceof Subtracao)
			return executaRecursivo(((Subtracao) arv).arg1) - executaRecursivo(((Subtracao) arv).arg2);
				
		if(arv instanceof Divisao)
			return executaRecursivo(((Divisao) arv).arg1) / executaRecursivo(((Divisao) arv).arg2);
				
		if (arv instanceof Num){
			return (float)((Num) arv).num;
		}
		return Float.valueOf("0.0");
	}
}
