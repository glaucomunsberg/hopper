import java.util.LinkedList;


public class MaquinaVirtual {
	enum TokenType{ NUM,SOMA, MULT,APar,FPar, EOF,DIVISAO,SUBTRACAO}
	private LinkedList<Float> values = new LinkedList<Float>();
	private Float result;
	
	public boolean executeCommand(String command, int Line){
		String bits[] = command.split(" ");
		float first,secound;
		if(bits.length < 2){
			switch(bits[0]){
				case "PRINT":
					System.out.println(values.removeFirst());
					return true;
				case "SUM":
					result = (values.removeFirst() + values.removeFirst());
					values.addFirst(result);
					return true;
				case "SUB":
					first = values.removeFirst();
					secound = values.removeFirst();
					result = (secound - first);
					values.addFirst(result);
					return true;
				case "MULT":
					first = values.removeFirst();
					secound = values.removeFirst();
					result = (first * secound);
					values.addFirst(result);
					return true;
				case "DIV":
					
					first = values.removeFirst();
					secound = values.removeFirst();
					if(secound == 0){
						System.out.println("Hooper: Divisão por zero!");
					}
					result = (secound/first);
					values.addFirst(result);
					return true;
				case "PUSH":
					
				default:
					System.out.println("Hopper: Commando não encontrado \""+command+"\"\n");
					return false;
			}
		}else{
			if(bits[0].equals("PUSH")){
				int value = Integer.parseInt(bits[1]);
				values.addFirst(((float)value));
			}else{
				System.out.println("Hopper: Comando não encontrado\""+command+"\"\n");
			}
		}
		return true;
	}
	
	public Float getResult(){
		return this.result;
	}
	
}
