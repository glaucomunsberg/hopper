DIGITO [0-9]
DIGITOS {DIGITO}+
LETRA [a-zA-Z]
FRACAO ("."{DIGITOS})?
SINAL "+" | "-"
SINALPOSITIVO "+"
SINALNEGATIVO "-"
EXP ("E"({SINAL})?{DIGITOS})?
NUM {DIGITOS}{FRACAO}{EXP}

DOISPONTOS ":"
IGUAL "="
MENORQUE "<"
MAIORQUE ">"
MENORIGUAL {MENORQUE}{IGUAL}
MAIORIGUAL {MAIORQUE}{IGUAL}
POSITIVO_SOMA "+"
NEGATIVO_SUBTRACAO "-"
OU (" ")+["o"]["u"](" ")+
MULT "*"
BARRA "/"
DIV (" ")+["d"]["i"]["v"](" ")+
MOD (" ")+["m"]["o"]["d"](" ")+
OP_AND (" ")+["a"]["n"]["d"](" ")+
ATRIBUICAO {DOISPONTOS}{IGUAL}
FUNCTION (" ")+["f"]["u"]["n"]["c"]["t"]["i"]["o"]["n"](" ")+
PROCEDURE (" ")+["p"]["r"]["o"]["c"]["e"]["d"]["u"]["r"]["e"](" ")+
TIPO_INTEIRO (" ")+["i"]["n"]["t"]["e"]["i"]["r"]["o"](" ")+
TIPO_REAL (" ")+["r"]["e"]["a"]["l"](" ")+
ID {LETRA}({LETRA}|{DIGITO})*
BEGIN (" ")+["b"]["e"]["g"]["i"]["n"](" ")+
END	(" ")+["e"]["n"]["d"](" ")+
NOT (" ")+["n"]["o"]["t"](" ")+
PROGRAM (" ")*["p"]["r"]["o"]["g"]["r"]["a"]["m"](" ")+
LBRACE "("
RBRACE ")"
SEMI ";"	
VIRG ","
COND_IF (" ")*["i"]["f"](" ")+
THEN	(" ")+["t"]["h"]["e"]["n"](" ")+
COND_ELSE	(" ")*["e"]["l"]["s"]["e"](" ")+
LOOP_WHILE	(" ")*["w"]["h"]["i"]["l"]["e"](" ")+
AUX_LOOP_DO	(" ")+["d"]["o"](" ")+
VAR	(" ")*["v"]["a"]["r"](" ")+
ARRAY (" ")+["a"]["r"]["r"]["a"]["y"](" ")+
DOT (" ")*["."](" ")*
ESPACE [\ \t\n]

%%

{FUNCTION}				{printf("Palavra reservada function: %s\n",yytext);}

{POSITIVO_SOMA}			{printf("Sinal positivo / Soma: %s\n",yytext);}
{NEGATIVO_SUBTRACAO}	{printf("Sinal negativo / Subtracao: %s\n",yytext);}
{END}					{printf("Palavra reservada End: %s\n",yytext);}
{BEGIN}					{printf("Palavra reservada Begin: %s\n",yytext);}
{NOT}					{printf("Negacao: %s\n",yytext);}
{ID}					{printf("Identificador: %s\n",yytext);}
{DOISPONTOS}			{printf("Dois pontos: %s\n",yytext);}
{BARRA}					{printf("Barra: %s\n",yytext);}
{MAIORQUE}				{printf("Operador Maior: %s\n",yytext);}
{MAIORIGUAL}			{printf("Operador Maior ou igual: %s\n",yytext);}
{MENORQUE}				{printf("Operador Menor: %s\n",yytext);}
{MENORIGUAL}			{printf("Operador Menor ou igual: %s\n",yytext);}	
{TIPO_REAL}				{printf("Float: %s\n",yytext);}
{TIPO_INTEIRO}			{printf("Inteiro: %s\n",yytext);}
{PROCEDURE}				{printf("Palavra reservada Procedure: %s\n",yytext);}
{ATRIBUICAO}			{printf("Operador de atribuicao: %\n",yytext);}
{OP_AND}				{printf("Palavra reservada and: %s\n",yytext);}
{MOD}					{printf("Palavra reservada modulo: %s\n",yytext);}
{MULT}					{printf("Palavra reservada mult: %s\n",yytext);}
{OU}					{printf("Palavra reservada ou: %s\n",yytext);}
{SEMI}					{printf("Ponto e virgula: %s\n",yytext);}
{LBRACE}				{printf("Abre parenteses: %s\n",yytext);}
{RBRACE}				{printf("Fecha parenteses: %s\n",yytext);}
{VIRG}					{printf("Virgula: %s\n",yytext);}
{COND_IF}				{printf("Palavra reservada if: %s\n",yytext);}
{THEN}					{printf("Palavra reservada then: %s\n",yytext);}
{COND_ELSE}				{printf("Palavra reservada else: %s\n",yytext);}
{LOOP_WHILE}			{printf("Palavra reservada while: %s\n",yytext);}
{AUX_LOOP_DO}			{printf("Palavra reservada do: %s\n",yytext);}
{ARRAY}				{printf("Palavra reservada array: %s\n",yytext);}	
{DOT}				{printf("Ponto: %s\n",yytext);}

%%


int yywrap();


main(){
  yylex();
}


int yywrap(){
 return 1;
}
