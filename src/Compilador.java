class Compilador{
	
	public static void main(String[]args)
	{	
		ArvoreSintatica arv=null;
		CodeGen backend;
		String codigo;
		MaquinaVirtual maquinaVirtual;
		if(args.length < 2){
			System.out.print(
					"Hopper: Numero de argumentos errados:\n"
				  + "<IN> [OPCAO]\n"
				  + "\n"
				  + "Opção	Significado\n"
				  + "-C		Compila o código\n"
				  + "-I		Interpreta o código\n"
				  + "-M		Executa o código na Máquina Virtual\n");
		}
		try{

			AnaliseLexica al = new AnaliseLexica(args[0]);
			Parser as = new Parser(al);
			arv = as.parseProg();
			
			switch(args[1]){
				case "-C":
				case "-c":
					backend = new CodeGen();
					codigo = backend.geraCodigo(arv);
					System.out.println(codigo);
					break;
				case "-M":
				case "-m":
					backend = new CodeGen();
					codigo = backend.geraCodigo(arv);
					String linhas[] = codigo.split("\n");
					maquinaVirtual = new MaquinaVirtual();
					
					for(int a=0; a < linhas.length; a++){
						maquinaVirtual.executeCommand(linhas[a], a);
					}
					
					break;
				case "-I":
				case "-i":
					Interpretador interpretador = new Interpretador();
					System.out.println(interpretador.executa(arv));
					break;
				
				default:
					System.out.println("Hopper: Opção não reconhecida \""+args[1]+"\"");
			}
			

		}catch(Exception e)
		{			
			System.out.println("Hopper: Erro de compilação:\n" + e);
			e.printStackTrace();
		}



	}
}
